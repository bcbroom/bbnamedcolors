# BBNamedColors

[![CI Status](http://img.shields.io/travis/Brian Broom/BBNamedColors.svg?style=flat)](https://travis-ci.org/Brian Broom/BBNamedColors)
[![Version](https://img.shields.io/cocoapods/v/BBNamedColors.svg?style=flat)](http://cocoadocs.org/docsets/BBNamedColors)
[![License](https://img.shields.io/cocoapods/l/BBNamedColors.svg?style=flat)](http://cocoadocs.org/docsets/BBNamedColors)
[![Platform](https://img.shields.io/cocoapods/p/BBNamedColors.svg?style=flat)](http://cocoadocs.org/docsets/BBNamedColors)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BBNamedColors is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "BBNamedColors", :git => 'https://bitbucket.org/bcbroom/bbnamedcolors'

## Author

Brian Broom, brian.broom@gmail.com

## License

BBNamedColors is available under the MIT license. See the LICENSE file for more info.