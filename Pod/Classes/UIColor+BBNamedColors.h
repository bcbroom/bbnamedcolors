//
//  UIColor+BBNamedColors.h
//  Pods
//
//  Created by Brian Broom on 7/23/14.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (BBNamedColors)

+ (instancetype)aliceBlue;
+ (instancetype)antiqueWhite;
+ (instancetype)aqua;

@end
