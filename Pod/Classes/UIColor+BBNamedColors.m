//
//  UIColor+BBNamedColors.m
//  Pods
//
//  Created by Brian Broom on 7/23/14.
//
//

#import "UIColor+BBNamedColors.h"

@implementation UIColor (BBNamedColors)

+ (instancetype)aliceBlue {
    return [UIColor colorWithRed:240.0/255 green:248.0/255 blue:255.0/255 alpha:1.0];
}

+ (instancetype)antiqueWhite {
    return [UIColor colorWithRed:250.0/255 green:235.0/255 blue:215.0/255 alpha:1.0];
}

+ (instancetype)aqua {
    return [UIColor colorWithRed:0.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
}

@end
