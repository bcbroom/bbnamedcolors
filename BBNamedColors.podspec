#
# Be sure to run `pod lib lint BBNamedColors.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "BBNamedColors"
  s.version          = "0.1.0"
  s.summary          = "Use CSS4 Named Colors from UIColor"
  s.description      = <<-DESC
                       Allows the use of CSS4 named colors as class methods on UIColor instead of having to specify RGB values.
                       ex [UIColor aqua]
                       DESC
  s.homepage         = "https://bitbucket.org/bcbroom/bbnamedcolors"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Brian Broom" => "brian.broom@gmail.com" }
  s.source           = { :git => "https://bcbroom@bitbucket.org/bcbroom/bbnamedcolors.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/bcbroom'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes'
  #s.resources = 'Pod/Assets/*.png'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
